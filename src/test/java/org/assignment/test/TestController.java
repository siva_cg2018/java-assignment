package org.assignment.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.*;

import com.assignment.controller.FriendsManagementController;
import com.assignment.dto.FriendsDTO;
import com.assignment.dto.NewFriendsDTO;
import com.assignment.dto.ResponseDTO;
import com.assignment.model.Blocked;
import com.assignment.model.Subscription;
import com.assignment.service.BlockServiceImpl;
import com.assignment.service.FriendsService;
import com.assignment.service.FriendsServiceImpl;
import com.assignment.service.SubscriptionServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

/*
 * @author Sivakumar Murugan
 */

@RunWith(MockitoJUnitRunner.class)
//@ComponentScan(basePackages = "com.assignment.controller")
public class TestController {
	
	MockMvc mockmvc;
	
	@InjectMocks
	FriendsManagementController controller;
	
	@Mock 
	FriendsServiceImpl friendsService;
	
	@Mock
	SubscriptionServiceImpl subscriptionService;
	
	@Mock
	BlockServiceImpl blockService;
	

	@Test
	public void succsesAddNewFriendsTest() {
		NewFriendsDTO friendsBean = new NewFriendsDTO();

		String[] emails = {"lisa@test.com", "andy@test.com"};
		friendsBean.setFriends(emails);
		
		friendsService.add(friendsBean);
		when(friendsService.add(friendsBean)).thenReturn(true);
		
		ResponseDTO expected = controller.newFriends(friendsBean);
		
		ResponseDTO actual = new ResponseDTO(true);
		
		Assert.assertEquals(toJSON(expected), toJSON(actual));
	}
	
	
	@Test
	public void successSubscribeUpdateTest(){
		
		Subscription subscribe = new Subscription();
		
		subscribe.setRequestor("lisa@test.com");
		subscribe.setTarget("john@test.com");
		
		subscriptionService.subsribe(subscribe);
		when(subscriptionService.subsribe(subscribe)).thenReturn(true);
		
		ResponseDTO expected = controller.subscribeUpdates(subscribe);
		
		ResponseDTO actual = new ResponseDTO(true);
		
		Assert.assertEquals(toJSON(expected), toJSON(actual));

	}

	
	@Test
	public void successBlockUpdateTest(){
		
		Blocked block = new Blocked();
		
		block.setRequestor("lisa@test.com");
		block.setTarget("john@test.com");
		
		blockService.block(block);
		when(blockService.block(block)).thenReturn(true);
		
		ResponseDTO expected = controller.blockUpdates(block);
		
		ResponseDTO actual = new ResponseDTO(true);
		
		Assert.assertEquals(toJSON(expected), toJSON(actual));

	}
	
	//@Test
	/*public void alreadFriendsTest()throws Exception{
		
		String[] emails = {"lisa@example.com", "andy@example.com"};
		
		NewFriendsDTO friends = new NewFriendsDTO();
		friends.setFriends(emails);
		
		friendsService.add(friends);
		when(friendsService.add(friends)).thenReturn(true);
		
		List<Friends> f = friendsService.checkIfRelationExist(emails[0], emails[1]);
		
		try{
			ResponseDTO actualResponse = controller.newFriends(friends);
		}catch(Exception e){
			
		}
		
		System.out.println(f);
		
		Assert.assertEquals(toJSON(actualResponse), toJSON( new ResponseDTO(true) ));
		
	}
	*/
	public  String toJSON(Object obj){
		try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
	}
}
		