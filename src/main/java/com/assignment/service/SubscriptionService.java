package com.assignment.service;

import com.assignment.dto.SubscribeDTO;
import com.assignment.model.Subscription;

public interface SubscriptionService {
	
	public Boolean subsribe(Subscription request);
}
