package com.assignment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.dao.UserDAO;
import com.assignment.model.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDao;
	
	@Override
	public void addUser(String email) {
		
		User user = new User();
		user.setEmail(email);
		userDao.addUser(user);
		
	}

}
