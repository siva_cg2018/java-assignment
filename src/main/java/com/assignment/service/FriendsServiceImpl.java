package com.assignment.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.assignment.constants.ErrorMessages;
import com.assignment.dao.BlockedDAO;
import com.assignment.dao.FriendsDao;
import com.assignment.dao.UserDAO;
import com.assignment.dto.FriendsDTO;
import com.assignment.dto.NewFriendsDTO;
import com.assignment.exception.DuplicateFriendsException;
import com.assignment.exception.InvalidEmailException;
import com.assignment.exception.InvalidEmailListException;
import com.assignment.exception.InvalidUserException;
import com.assignment.exception.UpdatesBlockedExeption;
import com.assignment.helpers.Validation;
import com.assignment.model.Friends;
import com.assignment.model.User;

import javax.validation.*;
import java.util.Set;

/*
 *  Service class related to Friends model
 * 
 *  @author - Sivakumar Murugan
 */


@Service
public class FriendsServiceImpl implements FriendsService {
	
	@Autowired
	private FriendsDao friendsDao; 
	
	@Autowired
	private BlockService blockedService;
	
	@Autowired
	private UserService userService;
	
	/*
	 * Method to add new friends 
	 * @param rln - NewFriendsDTO 
	 */

	 @Override
	 public Boolean add(NewFriendsDTO rln){
		 
		String[] friends = rln.getFriends();
		System.out.println(friends);
		
		Validation.checkEmailListSize(friends);
		
		String user1 = friends[0];
		String user2 = friends[1];
		
		// valid if email id provided is valid
		Validation.isValidEmail(user1);
		
		// valid if email id provided is valid
		Validation.isValidEmail(user2);
		
		// check if both email are not the same
		Validation.checkNotSame(user1, user2);
		
		
		// if already friends throw exception
		if(checkIfRelationExist(user1, user2).size()>0){
			throw new DuplicateFriendsException(ErrorMessages.ALREADY_FRIENDS_ERROR);
		}
		
		// Create user 1
		userService.addUser(user1);
		// Create user 2
		userService.addUser(user2);
		
		// check if user 1 has blocked user 2
		// check if user 2 has blocked user 1
		if(blockedService.checkIfBlocked(user1, user2)){
			
			throw new UpdatesBlockedExeption(user1, user2);
			
		}else if(blockedService.checkIfBlocked(user1, user2)){
			
			throw new UpdatesBlockedExeption(user2, user1);
		
		}
		
		Friends f = new Friends();
		f.setUser1(user1);
		f.setUser2(user2);
		
		// create new friends entry 
		friendsDao.add(f);
		
		return true;
	}

	 
	 /*
	  * (non-Javadoc)
	  * @see service.FriendsService#check_if_relation_exist(java.lang.String, java.lang.String)
	  * Check if2 email id has friends relation
	  * @param String user1 - email id
	  * @param String user2 - email id
	  * @returns instance of the record
	  */
	 
	 @Override
	 public List<Friends> checkIfRelationExist(String user1, String user2){
		return  friendsDao.checkIfRelationExist(user1, user2);
		//return records;
	 }
	 
	 
	 /*
	  * Check if 2 email id has friends relation
	  * @param FriendsDTO friend - FriendsDTO with property email 
	  * @returns instance of the record
	  */
	 
	 @Override
	 public ArrayList<String> getFriendList(FriendsDTO friend){
		 
		 ArrayList<String> friend_email_ids = null;
		 
		// Get email from input json
		String email = friend.getEmail();
		
		// Clean email text
		email = email.trim();
		
		
		// validate if email id provided is valid
		 Validation.isValidEmail(email);
				
		
		 // Get list Friend records for the user  
		 List<Friends> record = friendsDao.getFriendsList(email);
		
		 // get common email among both friend list list
		 if(record.size()>0){
			friend_email_ids = get_friend_email_ids(record, email);
		 }else{
			 throw new InvalidUserException(email);
		 }
		 return friend_email_ids; 
	 }
	 
	 
	 
	 /*
	  * Get email id from friends record
	  * @param List<Friends> record - List of friends record
	  * @param String user2 - email id in request
	  * @returns ArrayList<String> of the email id
	  */
	 
	 public static ArrayList<String> get_friend_email_ids(List<Friends> record, String email){
		 
		 ArrayList<String> email_ids = new ArrayList<String>();
		 String user1, user2;
		 
		 for(Friends r: record){
				user1 = r.getUser1();
				user2= r.getUser2();
				
				if(email.equals(user1)){
					email_ids.add(user2);
				}else if(email.equals(user2)){
					email_ids.add(user1);
				}
		 }
		 
		 return email_ids;
	 }
	 
	 
	 /*
	  * Get common friends between 2 users
	  * @param friends - String list of 2 user's email id
	  * @returns ArrayList<String> of common email id
	  */
	 
	 public ArrayList<String> get_common_friends(NewFriendsDTO friends) throws Exception{
		 
		 ArrayList<String> common_friends= null;
		 String[] _friends = friends.getFriends();
		 
		 // Validate if 2 input email provided
		 Validation.checkEmailListSize(_friends);
		 
		 try{
			 // Get friend list of both email ids 
			 List<Friends> user1_friends = friendsDao.getFriendsList(_friends[0]);
			 List<Friends> user2_friends = friendsDao.getFriendsList(_friends[1]);
			 
			 // Get only email ids from friends record
			 ArrayList<String> email_ids_1 = get_friend_email_ids(user1_friends, _friends[0]);
			 //System.out.println("email_ids_1 :"+email_ids_1);
			 ArrayList<String> email_ids_2 = get_friend_email_ids(user2_friends, _friends[1]);
			 //System.out.println("email_ids_1 :"+email_ids_2);
			 
			 // Find common email id among 2
			 if(email_ids_1.size() > email_ids_2.size()){
				email_ids_1.retainAll(email_ids_2);
				common_friends = email_ids_1;
			 }else{
				email_ids_2.retainAll(email_ids_1);
				common_friends = email_ids_2;
			 }
			 
		 }catch(Exception e){
			 
		 }
		 
		 return common_friends;
	 }
	 
}
