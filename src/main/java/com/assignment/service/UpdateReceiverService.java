package com.assignment.service;

import java.util.ArrayList;

import com.assignment.dto.UpdatesReceiverListDTO;

public interface UpdateReceiverService {

	public ArrayList<String> getUpdatesRecipients(UpdatesReceiverListDTO request);
	
}
