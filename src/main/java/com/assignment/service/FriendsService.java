package com.assignment.service;

import java.util.ArrayList;
import java.util.List;

import com.assignment.dto.FriendsDTO;
import com.assignment.dto.NewFriendsDTO;
import com.assignment.model.Friends;

public interface FriendsService {
	public Boolean add(NewFriendsDTO r);
	
	// Check if relation between 2 email id already exists if yes then return
	public List<Friends> checkIfRelationExist(String user1, String user2);
	
	// Get friend list of an user/emailid
	public ArrayList<String> getFriendList(FriendsDTO friend);
	
	public ArrayList<String> get_common_friends(NewFriendsDTO friends) throws Exception;	
}
