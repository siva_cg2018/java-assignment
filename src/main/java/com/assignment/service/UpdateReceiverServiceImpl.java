package com.assignment.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.dao.BlockedDAO;
import com.assignment.dao.FriendsDao;
import com.assignment.dao.SubscriptionDAO;
import com.assignment.dto.UpdatesReceiverListDTO;
import com.assignment.exception.InvalidEmailException;
import com.assignment.helpers.Validation;
import com.assignment.model.Blocked;
import com.assignment.model.Friends;
import com.assignment.model.Subscription;

/*
 * Service class to get list of email who will receive updates from sender email
 *
 * @author - Sivakumar Murugan
 */

@Service
public class UpdateReceiverServiceImpl implements UpdateReceiverService  {

	@Autowired
	FriendsDao relationDao;
	
	@Autowired 
	SubscriptionDAO subscriptionDao;
	
	@Autowired
	BlockedDAO blockedDao;
	
	/*
	 * (non-Javadoc)
	 * @see service.UpdateReceiverService#get_updates_receiver_email_id(dto.UpdatesReceiverListDTO)
	 *
	 * Method to get list of email who will receive updates from sender email
	 * 
	 * @param UpdatesReceiverListDTO request - input JSON with property sender  & test
	 * @returns ArryaList<String> who receives updates from sender
	 *
	 */
	
	
	public ArrayList<String> getUpdatesRecipients(UpdatesReceiverListDTO request){
		
		ArrayList<String> updates_receivers = new ArrayList<>();
		
		String sender = request.getSender();
		String text = request.getText();
		
		
		// Validate sender email
		Validation.isValidEmail(sender);
				
		// Get friend list for sender email
		List<Friends> friend_list = relationDao.getFriendsList(sender);
		
		// Friend list of sender
		ArrayList<String> friend_list_emailids = FriendsServiceImpl.get_friend_email_ids(friend_list, sender);
		System.out.println("friend_list_emailids :"+friend_list_emailids);
		
		// List of email subscribed for updates from sender
		List<Subscription> subscribers_list = subscriptionDao.subscriptionList(sender);
		// use Java 8 map functionality to get only requestor email from List<Subscription> subscribers_list
		List<String> subscriber_email_list = subscribers_list.stream().map(s -> s.getRequester() ).collect(Collectors.toList());;		
		
		// List of email blocked updates from sender
		List<Blocked> blocked_list = blockedDao.blockedList(sender);
		// use Java 8 map functionality to get only requestor email from List<Subscription> subscribers_list
		List<String> blocked_list_email = blocked_list.stream().map(b -> b.getRequestor() ).collect(Collectors.toList());
		
		// Email mentioned in text
		ArrayList<String> email_in_text = parse_email_from_text(text);
		System.out.println("email_in_text :"+email_in_text);
		
		// check if a friend is blocked, if not consider it as update receiver 
		for(String e: friend_list_emailids){
			if(!blocked_list_email.contains(e)){
				updates_receivers.add(e);
			}
		}
		
		// check if a subscriber is blocked, if not consider it as update receiver
		for(String e: subscriber_email_list){
			if(!blocked_list_email.contains(e) && !updates_receivers.contains(e)){
				updates_receivers.add(e);
			}
		}
		
		// check if email_in_text has blocked email, if not consider it as update receiver
		for(String e: email_in_text){
			if(!blocked_list_email.contains(e) && !updates_receivers.contains(e)){
				updates_receivers.add(e);
			}
		}
		
		return updates_receivers;
	}
	
	/*
	 *  Method to get email from text
	 *  @param String text - text 
	 *  
	 *  @returns ArrayList<String> of email found in text
	 */
	
	public final ArrayList<String> parse_email_from_text(String text){
		
		// Get email mentioned in comment
		ArrayList<String> email_found = Validation.parseText(text);
		return email_found;
	}
}
