package com.assignment.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.constants.ErrorMessages;
import com.assignment.dao.SubscriptionDAO;
import com.assignment.exception.InvalidEmailException;
import com.assignment.helpers.Validation;
import com.assignment.model.Subscription;

/*
 *  Service class to handle subscription related functionalities
 *  All activities related to Entity Subscription are performed here
 */

@Service
public class SubscriptionServiceImpl implements SubscriptionService {
	
	@Autowired
	private SubscriptionDAO subscriptionDAO; 
	
	@Autowired
	private UserService userSerive;
	
	/*
	 * (non-Javadoc)
	 * @see service.SubscriptionService#subsribe(model.Subscription)
	 * Method to add new subscription
	 * @Param Subscription request - has two properties requester & target
	 * Return Boolean true/false
	 */
	
	
	@Override
	public Boolean subsribe(Subscription request){
		
		String requestor = request.getRequester();
		String target = request.getTarget();
		
		/*if(requester==null || target==null){
			throw new InvalidEmailException("Requestor or target missing");
		}*/
		
		// Validate requester email
		Validation.isValidEmail(requestor);
		
		// Validate target email
		Validation.isValidEmail(target);
		
		// Add new user
		userSerive.addUser(requestor);
		userSerive.addUser(target);
		
		if(subscriptionDAO.checkAlreadySubscribed(request)){
			throw new InvalidEmailException(ErrorMessages.ALREADY_SUBSCRIBED_ERROR);
		}
		subscriptionDAO.subscribe(request);
		
		return true;
	}
}
