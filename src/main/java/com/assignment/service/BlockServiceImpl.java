package com.assignment.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.constants.ErrorMessages;
import com.assignment.dao.BlockedDAO;
import com.assignment.exception.DuplicateBlockException;
import com.assignment.helpers.Validation;
import com.assignment.model.Blocked;

/*
 *  Service class related to Block entity
 *  
 *  @author - Sivakumar Murugan
 */

@Service
public class BlockServiceImpl implements BlockService{
	
	@Autowired
	private BlockedDAO blockDao; 
	
	/*
	 * (non-Javadoc)
	 * @see service.BlockService#block(model.Blocked)
	 * Method to block a email from getting updates from another email
	 * @param Bolcked request - request with property requestor and target 
	 */
	
	@Override
	public Boolean block(Blocked request){
		
		String requestor = request.getRequestor();
		String target = request.getTarget();
		
		
		// Validate requester email
		Validation.isValidEmail(requestor);
		
		// Validate target email
		Validation.isValidEmail(target);
		
		// check if already blocked, if blocked throw exception
		if(blockDao.checkIfBolcked(request)){
			throw new DuplicateBlockException(ErrorMessages.ALREADY_BLOCKED_ERROR);
		}
		
		blockDao.block(request);
		
		return true;
	}
	
	
	/*
	 *  Method to check if user 1 has blocked user 2
	 *  @param String user1 - requestor email id
	 *  @param String user2 - target email id
	 *  
	 *  @returns true if blocked else false
	 */
	
	@Override
	public Boolean checkIfBlocked(String user1, String user2){
		
		// create new Blocked class object
		
		Blocked b = new Blocked();
		
		// set requestor and target
		b.setRequestor(user1);
		b.setTarget(user2);
		
		// check if user 1 has blocked user 2, return true if yes else false
		return blockDao.checkIfBolcked(b);
	}
}
