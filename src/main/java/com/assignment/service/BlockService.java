package com.assignment.service;


import com.assignment.model.Blocked;

public interface BlockService {
	public Boolean block(Blocked request);
	public Boolean checkIfBlocked(String user1, String user2);
}
