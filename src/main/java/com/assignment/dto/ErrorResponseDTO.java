package com.assignment.dto;

public class ErrorResponseDTO extends ResponseDTO {
	
	private String message;
	private String error_code;
	
	public ErrorResponseDTO(String message, String error_code){
		setSuccess(false);
		setMessage(message);
		setError_code(error_code);
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getError_code() {
		return error_code;
	}
	public void setError_code(String error_code) {
		this.error_code = error_code;
	}
}
