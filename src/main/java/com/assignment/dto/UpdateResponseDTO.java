package com.assignment.dto;

import java.util.ArrayList;

public class UpdateResponseDTO extends ResponseDTO{
	
	private ArrayList<String> recipients;
	
	public UpdateResponseDTO(ArrayList<String> recipients){
		setSuccess(true);
		setRecipients(recipients);
	}

	public ArrayList<String> getRecipients() {
		return recipients;
	}

	public void setRecipients(ArrayList<String> recipients) {
		this.recipients = recipients;
	}
}
