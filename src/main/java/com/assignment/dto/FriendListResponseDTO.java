package com.assignment.dto;

import java.util.ArrayList;

public class FriendListResponseDTO extends ResponseDTO {
	private ArrayList<String> friends;
	
	private int count;
	
	public FriendListResponseDTO(ArrayList<String> friends, int count){
		setSuccess(true);
		setFriends(friends);
		setCount(count);
	}
	
	public ArrayList<String> getFriends() {
		return friends;
	}
	public void setFriends(ArrayList<String> friends) {
		this.friends = friends;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	
}
