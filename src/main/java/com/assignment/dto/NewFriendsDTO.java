package com.assignment.dto;

import javax.validation.constraints.NotNull;

public class NewFriendsDTO {
	
	@NotNull
	private String[] friends = new String[2];

	public String[] getFriends() {
		return friends;
	}

	public void setFriends(String[] friends) {
		this.friends = friends;
	}
	
	
}
