package com.assignment.dto;



public class ResponseDTO {
	private Boolean success;
	//private String  message;
	
	public ResponseDTO(){
		
	}
	
	public ResponseDTO(Boolean success){
		setSuccess(success);
	}
	
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
}
