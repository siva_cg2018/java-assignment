package com.assignment.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.springframework.format.annotation.NumberFormat;

/*
 *  Entity bean to perform SQL action on table tbl_relation
 */

@Entity(name = "Relation")
@Table(name="tbl_friends")
public class Friends {

	@Id
   @GeneratedValue
   @Column(name = "id")	
	
	private long id;
	
	@NotNull
	@Email
	@Size(max=200)
	@Column(name="user1")
	private String user1= "";
	
	@NotNull
	@Email
	@Size(max=200)
	@Column(name="user2")
	private String user2= "";
	
	/*
	@Column(name="are_friends")
	private int are_friends;
	
	@Column(name="subscribed")
	private int subscribed;
	
	
	@Email
	@Column(name="subscribed_by")
	private String subscribed_by;
	
	@Column(name="blocked")
	public int blocked=0;
	*
	
	
	public String getBlocked_by() {
		return blocked_by;
	}

	public void setBlocked_by(String blocked_by) {
		this.blocked_by = blocked_by;
	}

	public String getSubscribed_by() {
		return subscribed_by;
	}

	public void setSubscribed_by(String subscribed_by) {
		this.subscribed_by = subscribed_by;
	}

	@Email
	@Column(name="blocked_by")
	private String blocked_by;
	*/
	
	public void setUser1(String user1){
		this.user1= user1;
	}
	
	public String getUser1(){
		return this.user1;
	}
	
	public void setUser2(String user2){
		this.user2= user2;
	}
	
	public String getUser2(){
		return this.user2;
	}
	
	/*
	public void setAreFriends(int are_friends){
		this.are_friends = are_friends;
	}
	
	public int getAreFriends(){
		return this.are_friends;
	}
	
	public void setSubscribed(int subscribed){
		this.subscribed = subscribed;
	}
	
	public int getSubscribed(){
		return this.subscribed;
	}
	
	public void setBlocked(int blocked){
		this.blocked = blocked;
	}
	
	public int getBlocked(){
		return this.blocked;
	}
	
	public void setBlockedBy(String blocked_by){
		this.blocked_by= blocked_by;
	}
	
	public String getBlockedBy(){
		return this.blocked_by;
	}
	*/
}
