package com.assignment.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.constants.ErrorMessages;
import com.assignment.dao.FriendsDao;
import com.assignment.dao.FriendsDaoImpl;
import com.assignment.dto.ErrorResponseDTO;
import com.assignment.dto.FriendListResponseDTO;
import com.assignment.dto.FriendsDTO;
import com.assignment.dto.NewFriendsDTO;
import com.assignment.dto.ResponseDTO;
import com.assignment.dto.SubscribeDTO;
import com.assignment.dto.TestInput;
import com.assignment.dto.UpdateResponseDTO;
import com.assignment.dto.UpdatesReceiverListDTO;
import com.assignment.exception.InvalidEmailException;
import com.assignment.exception.InvalidEmailListException;
import com.assignment.exception.InvalidUserException;
import com.assignment.helpers.CleanInput;
import com.assignment.model.Blocked;
import com.assignment.model.Friends;
import com.assignment.model.Subscription;
import com.assignment.service.BlockService;
import com.assignment.service.FriendsService;
import com.assignment.service.SubscriptionService;
import com.assignment.service.UpdateReceiverService;


/*
 * Controller for all the API 
 * Handles all the API request
 * Access URL path /RESTAPI/
 * 
 * @author - Sivakumar Murugan
 */


@RestController
public class FriendsManagementController {
	
	
	
	@Autowired
	private FriendsService relationService;
	
	@Autowired
	private SubscriptionService subscriptionService;
	
	@Autowired
	private BlockService blockService;
	
	@Autowired
	private UpdateReceiverService updRecvrService;
	
	private Logger Log = Logger.getLogger(FriendsManagementController.class);
	
	
	/*
	 *  Handler for creating new friends connection 
	 * 	@URL : "/new_friends"
	 * 	@param NewFriendsDTO request - input request of type NewFriendsJSON with friends property
	 * 	@returns ResponseDTO 
	 */
	
	@PostMapping(value="/new_friends")
	public ResponseDTO newFriends(@Valid @RequestBody NewFriendsDTO request){
		
		Log.info("API /new_friends :: INPUT => (1)"+request.getFriends()[0]+"  (2)"+request.getFriends()[1] );
		
		relationService.add(request);
		
		Log.info("API /new_friends exit");
		
		return new ResponseDTO(true);
	}
	
	
	/*
	 *  Handler to fetch friend list for a particular user
	 *  Exception thrown is provided email doesn't exist 
	 * 	@URL = "/friends_list"
	 * 	@param request - input request of type FriendsDTO with email property
	 * 
	 *  @throws InvalidUserException if email not found
	 *  @throws InvalidEmailException for invalid email id
	 * 	@throws UserBlcokeException if any one has blocked update from other 
	 * 
	 *  @return list of friends in FriendListResponseDTO format
	 */
	
	@PostMapping("/friends_list")
	public ResponseDTO friendsList(@Valid @RequestBody FriendsDTO request){
		
		Log.info("Mapped API /friends_list  INPUT  => "+request.getEmail() );
		
		ArrayList<String> friend_list = relationService.getFriendList(request);
		
		Log.info("Exit API /friends_list");
		
		return new FriendListResponseDTO(friend_list, friend_list.size());
	}

	
	/*
	 *  Handler to get common friends between 2 user email id
	 *  @param NewFriendsDTO friends - Input request of type NewFriendsDTO
	 *  
	 *  @URL = "/get_common_friends"
	 *  
	 *  @throws InvalidUserException if email not found
	 *  @throws InvalidEmailException for invalid email id
	 *  
	 *  @returns FriendsListResponseDTO - List of common email ids
	 */
	
	@PostMapping("/get_common_friends")
	public ResponseDTO getCommonFriends(@Valid @RequestBody NewFriendsDTO friends) throws Exception{
		
		  Log.info("Mapped API /friends_list  INPUT  => (1)- "+friends.getFriends()[0]+" (2)- "+friends.getFriends()[0] );
		
		  ArrayList<String> common_friends =relationService.get_common_friends(friends);
		  
		  Log.info("Exit API /get_common_friends");
		  
		  return new FriendListResponseDTO(common_friends, common_friends.size());
		  
		  
	}
	
	
	/*
	 *  Handler to add new subscription for a user
	 *  @URL = "/subscribe_updates"
	 *  @param Subscription request - Input JSON of type Subscription with property requestor & target
	 *  
	 *  @throws InvalidEmailException, AlreadySubscribedException
	 *  
	 *  @returns ResponseDTO 
	 */
	
	@PostMapping("/subscribe_updates")
	public ResponseDTO subscribeUpdates(@Valid @RequestBody Subscription request){
		
		Log.info("Mapped API /friends_list  INPUT  => @sender - "+request.getRequester()+" @target - "+request.getTarget() );
		  
		subscriptionService.subsribe(request);
		
		Log.info("Exit API /subscribe_updates");
		
		return new ResponseDTO(true);
	
	}
	
	
	/*
	 * Handler to block for updates from email
	 * @URL = "/block_updates"
	 * @param Blocked request - Input JSON of type Blocked with property requestor & target
	 * 
	 * @throws IvalidEmailException
	 * 
	 * @returns ResponseDTO
	 */
	
	@PostMapping("/block_updates")
	public ResponseDTO blockUpdates(@Valid @RequestBody Blocked request){
		
		Log.info("Mapped API /block_updates  INPUT  => @sender - "+request.getRequestor()+" @target- "+request.getTarget());
		  
		blockService.block(request);
		
		Log.info("Exit API /block_updates");
		
		return new ResponseDTO(true);
		
	}
	
	
	/*
	 *  Handler to get list of email who can receive update notification from particular user email
	 *  @URL = "/update_receiver_list"
	 *  @param UpdatesReceiverListDTO request - input request of type UpdatesReceiverListDTO
	 *  
	 *  @throws IvalidEmailException
	 *  
	 */
	
	@PostMapping("/send_updates")
	public ResponseDTO sendUpdates(@Valid @RequestBody UpdatesReceiverListDTO request){
		
		Log.info("Mapped API /send_updates  INPUT @sender - "+request.getSender()+" @target - "+request.getText() );	
		
		ArrayList<String> update_recvr_list = updRecvrService.getUpdatesRecipients(request);
		
		Log.info("Exit API /send_updates");
		
		return new UpdateResponseDTO(update_recvr_list);
		
	}
	
	
	/*
	 *  Handler for invalid API URLs and invalid request Method
	 *  @returns ResponseEntity with HTTP Status 404
	 */
	
	@RequestMapping("/*")
	public ResponseEntity<ErrorResponseDTO> notFound(HttpServletRequest request){	
		
		Log.info(" UNDEFINED URL Requested :: "+request.getMethod()+ " "+request.getRequestURI());	
		return  ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponseDTO(request.getPathInfo()+" "+ErrorMessages.NOT_FOUND, "404"));
	}

}
	