package com.assignment.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.assignment.constants.ErrorMessages;
import com.assignment.constants.OtherConstants;
import com.assignment.exception.InvalidEmailException;
import com.assignment.exception.InvalidEmailListException;

/*
 *  Validation class with methods to validate API request input
 *  
 *  @author - Sivakumar Murugan
 *  
 */

public class Validation{
	
	/*
	 * Method to check if 2 email id are not the same
	 *  @param String user1 - 1st email id
	 *  @param String user2 - 2nd email id
	 *  
	 *  @throws InvalidEmailException exception if condition is not met
	 */
	
	
	public static void checkNotSame(String user1, String user2){
		if(user1.trim().equals(user2.trim())){
			throw new InvalidEmailException(ErrorMessages.SAME_EMAIL_ID_ERROR);
		}
	}
	
	
	/*
	 * Method to validate email, 
	 * @validates - if not null
	 * @validates - if proper email id
	 * 
	 * @param String user - email id
	 * @throws InvalidEmailException if conditions not met
	 * 
	 */
	
	public void validateUser(String user){
		
		if(user == null){
			throw new InvalidEmailException(ErrorMessages.EMAIL_ID_REQUIRED);
		}
		
		if(!isValidEmail(user)){
			throw new InvalidEmailException(ErrorMessages.INVALID_EMAIL);
		}
	}
	
	
	/*
	 * Method to check email list of size 2
	 * @param - String[] friends - String array of friends email
	 * @throws exception if not of size 2
	 */
	
	public static void checkEmailListSize(String[] friends){
		
		if(friends.length != 2){
			 throw new InvalidEmailListException( new Integer(friends.length).toString() );
		 }
	}
	
	
	/*
	 * Method to validate if email is of valid format using regex
	 * @param String user -  email id
	 * @returns true on valid, false on invalid
	 */
	
	public static Boolean isValidEmail(String user){
		
		Pattern pattern = Pattern.compile(OtherConstants.email_regex);
		
		if(user == null)
			return false;
		
		return pattern.matcher(user).matches();
	}
	
	
	/*
	 * Method to parse email id from text
	 * @param String text - any text with 0  or more email id mentioned
	 * @returns ArrayList<String> of valid email ids
	 */
	
	public static  ArrayList<String> parseText(String text){
		
		ArrayList<String> email_ids = new ArrayList<String>();
		
		// Merge multi line text into one , and Split text based on space
		String[] split_text = text.replace("\n", " ").split(" ");
		
		// Check if each split word is valid email
		for(String word :  split_text){
			if(word!=null){
				word =  word.trim();
				if(isValidEmail(word)){
					email_ids.add(word);
				}
			}
		}
		
		return email_ids;
	}
	
}