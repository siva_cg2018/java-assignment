package com.assignment.configuration;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.assignment.configuration.ApplicationConfiguration;


public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
 @Override
 protected Class < ? > [] getRootConfigClasses() {
  return new Class[] {
   HibernateConfig.class
  };
 }
 @Override
 protected Class < ? > [] getServletConfigClasses() {
	 return new Class[] {
			   ApplicationConfiguration.class
			  };
 }
 @Override
 protected String[] getServletMappings() {
  return new String[] {
   "/*"
  };
 }
}