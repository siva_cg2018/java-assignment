package com.assignment.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.assignment.model.Blocked;
import com.assignment.model.Subscription;

@Configuration
@PropertySource("classpath:sql_queries.properties")

@Repository
public class BlockedDAOImpl implements BlockedDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private Environment sql;
	
	@Transactional
	@Override
	public void block(Blocked request) {
		sessionFactory.getCurrentSession().save(request);
	}

	@Transactional
	@Override
	public Boolean checkIfBolcked(Blocked request) {
		
		String requestor = request.getRequestor();
		String target = request.getTarget();
		
		Query query = sessionFactory.getCurrentSession().createQuery(sql.getProperty("blocked_list"));
		query.setString(0, requestor);
		query.setString(1, target);
		
		if(query.list().size()>0){
			return true;
		}
		
		return false;
	}
	
	
	@Transactional 
	@Override
	public List<Blocked> blockedList(String requestor){
		
		List<Blocked> subscription_list= null;
		
		try{
			Query query = sessionFactory.getCurrentSession().createQuery(sql.getProperty("blocked_list_target"));
			query.setString(0, requestor);
		
			subscription_list = query.list();
		}catch(HibernateException e){
			throw new HibernateException(e.getMessage());
		}
		
		return subscription_list;
		
	}

}
