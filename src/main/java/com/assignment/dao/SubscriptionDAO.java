package com.assignment.dao;

import java.util.List;

import com.assignment.model.Subscription;

public interface SubscriptionDAO {
	
	public void subscribe(Subscription request);
	
	public Boolean checkAlreadySubscribed(Subscription request);
	
	public List<Subscription> subscriptionList(String requestor);
}
