package com.assignment.dao;

import java.util.List;

import com.assignment.model.Blocked;

public interface BlockedDAO {
	public void block(Blocked request);
	public Boolean checkIfBolcked(Blocked request);
	public List<Blocked> blockedList(String requestor);
}
