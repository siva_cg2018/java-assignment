package com.assignment.dao;

import java.util.List;

import com.assignment.model.Friends;

public interface FriendsDao {
	 void add(Friends r);
	 List<Friends> checkIfRelationExist(String user1, String user2);
	 List<Friends> getFriendsList(String user);
	 void updateSubscription(Friends r);
}
