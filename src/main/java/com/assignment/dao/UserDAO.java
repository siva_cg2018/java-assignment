package com.assignment.dao;

import com.assignment.model.User;

public interface UserDAO {
	public Boolean checkIfUserExist(String email);
	public void addUser(User user);
}
