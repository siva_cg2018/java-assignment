package com.assignment.dao;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.assignment.model.User;

@Configuration
@PropertySource("classpath:sql_queries.properties")

@Repository
public class UserDAOImpl implements UserDAO {

	
	@Autowired
	private SessionFactory sf;
	
	@Autowired
	private Environment sqls;
	
	Logger log = Logger.getLogger(UserDAOImpl.class);
	
	@Override
	@Transactional
	public Boolean checkIfUserExist(String email) {
		
		log.info(" check_if_user_exist() :: "+email);
		
		// check user with particular email
		Query  query = sf.getCurrentSession().createQuery(sqls.getProperty("user_row"));
		query.setString(0, email);
		
		return (query.list().size() > 0 ? true : false );
		
	}

	@Override
	@Transactional
	public void addUser(User user) {
		// TODO Auto-generated method stub
		
		String email = user.getEmail();
		if(!checkIfUserExist(email)){
			
			log.info(" check_if_user_exist() :: user doesn't exist.");
			log.info(" add_user() :: create user.");
			
			try{
				sf.getCurrentSession().save(user);
				log.info(" add_user() :: user created");
			}catch(HibernateException e){
				log.error(" add_user :: exception "+e.getMessage());
			}
		}else{
			log.info(" add_user() :: "+email+" already exist.");
		}
		
	}

}
