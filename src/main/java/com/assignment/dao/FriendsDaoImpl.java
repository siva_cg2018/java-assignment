package com.assignment.dao;
import org.hibernate.SessionFactory;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.assignment.model.Friends;

@Configuration
@PropertySource("classpath:sql_queries.properties")


@Repository
public class FriendsDaoImpl implements FriendsDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private Environment sql;
	 
	//private SessionFactory sf;
	
	/*
	public void setSessionFactory(SessionFactory sessionFactory) {
        this.sf = sessionFactory;
    }*/
	
	//@Override
	@Transactional
	public void add(Friends r){
		sessionFactory.getCurrentSession().save(r);
	}
	
	/*
	 * Check if relation between 2 user exits
	 * @Param user1 - String email id
	 * @Param user2 - String email id
	 * Return instance of record found else null
	 * @see dao.FriendsDao#check_if_relation_exist(java.lang.String, java.lang.String)
	 */
	
	@Transactional
	public List<Friends> checkIfRelationExist(String user1, String user2){
		
		
		//try{
			Query query = sessionFactory.getCurrentSession().createQuery(sql.getProperty("check_if_relation_exists"));
			query.setString(0, user1);
			query.setString(1, user2);
			query.setString(2, user2);
			query.setString(3, user1);
			List<Friends> records = query.list();
			
			return records;
		/*	
		}catch(HibernateException e){
			//e.printStackTrace();
			//throw new HibernateException(e.getMessage());
		}
		List<Friends> records =null;
		return records;*/
	}
	
	@Transactional
	public List<Friends> getFriendsList(String user){
		List<Friends> records= null;
		try{
			
			Query query = sessionFactory.getCurrentSession().createQuery(sql.getProperty("friends_list"));
			query.setString(0, user);
			query.setString(1, user);
			
			records = query.list();
			
		}catch(HibernateException e){
			e.printStackTrace();
		}
		return records;
	}
	
	
	@Transactional
	@Override
	public void updateSubscription(Friends r){
		sessionFactory.getCurrentSession().update(r);
	}
	
}
