package com.assignment.constants;

/*
 *  Class Constants with all error messages
 *  
 *  @author - Sivakumar Murugan
 */

public class ErrorMessages {
	
	
	public static final String NOT_FOUND = "Requested resource doesn't exist";
	
	public static final String INVALID_INPUT = "Invalid request body";
	
	public static final String HIBERNATE_EXCEPTION = "Internal server Serror : DB manipulaion issue";
	
	public static final  String ALREADY_FRIENDS_ERROR = "Already friends";
	
	public static final String INTERNAL_SERVER_ERROR = "Internal server Serror";
	
	public static final String SAME_EMAIL_ID_ERROR = "Both the email ids cannot be same";
	
	public static final String INVALID_USER = "User doesn't exist - ";
	
	public static final String EMAIL_ID_REQUIRED = "Email id cannot be blank";
	
	public static final String INVALID_EMAIL = "Invalid email id format";
	
	public static final String INVALID_EMAIL_LIST = "2 email id required, received -";
	
	public static final String ALREADY_SUBSCRIBED_ERROR = "Already subscribed - ";
	
	public static final String ALREADY_BLOCKED_ERROR = "Already blocked - ";

	public static final String USER_BLOCKED = " has blocked ";
	
	public static final String NEW_FRIENDS_ERROR = " Friend request cannot be processed";
	
	
}
