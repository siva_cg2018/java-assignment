package com.assignment.constants;

/*
 * Constants class with all error codes
 * 
 * @author - Sivakumar Murugan
 */

public class ErrorCodes {
	
	public static final String PAGE_NOT_FOUND = "404";
	public static final String INVALID_INPUT = "401";
	
	public static final String INTERNAL_SERVER_ERROR = "500";
	
	public static final String INVALID_EMAIL_INPUT = "40001";
	
	public static final String INVALID_USER = "40004";
	
	public static final String ALREADY_FRIENDS_ERROR = "40011";
	public static final String ALREADY_SUBSCRIBED_ERROR = "40012";
	public static final String ALREADY_BLOCKED_ERROR = "40013";
	public static final String USER_BLOCKED = "40015";
	
	
	
}
