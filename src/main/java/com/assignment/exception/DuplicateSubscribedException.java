package com.assignment.exception;

import com.assignment.constants.ErrorCodes;

public class DuplicateSubscribedException extends BaseException {


	private static final long serialVersionUID = 6072681758775982047L;

	public DuplicateSubscribedException(String message) {
		super(message);
	}
	
	public String getErrorCode(){
		return ErrorCodes.ALREADY_SUBSCRIBED_ERROR;
	}

}
