package com.assignment.exception;

import com.assignment.constants.ErrorCodes;
import com.assignment.constants.ErrorMessages;

/*
 *  Exception class for invalid user
 *  Invalid user are the one that doesn't exist in the DB
 */

public class InvalidUserException extends BaseException {

	private static final long serialVersionUID = 1L;

	public InvalidUserException(String message){
		super(ErrorMessages.INVALID_USER+" "+message);
	}
	
	@Override
	public String getErrorCode(){
		return ErrorCodes.INVALID_USER;
	}
}
