package com.assignment.exception;



import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.assignment.constants.ErrorCodes;
import com.assignment.constants.ErrorMessages;
import com.assignment.dto.ErrorResponseDTO;
import com.assignment.exception.DuplicateBlockException;
import com.assignment.exception.DuplicateFriendsException;
import com.assignment.exception.DuplicateSubscribedException;
import com.assignment.exception.InvalidEmailException;
import com.assignment.exception.InvalidEmailListException;
import com.assignment.exception.InvalidUserException;
import com.assignment.exception.UpdatesBlockedExeption;

/*
 * GlobalException handler class
 * Handlers for all the different exception thrown in the code
 * 
 * @author - Sivakumar Murugan
 */

@ControllerAdvice
public class GlobalExceptionHandler  {

	private Logger log = Logger.getLogger(GlobalExceptionHandler.class);
	
	/*
	 * Exception handler for invalid request
	 */
	
	
	/*@ResponseStatus(value=HttpStatus.BAD_REQUEST)
    @ResponseBody
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e, HttpHeaders h, HttpStatus s, WebRequest r){
		
		//System.out.println("Invalid exception..");
		//log.error("Exception :: InvalidRequest Exception :: ");
		//return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponseDTO(ErrorMessages.INVALID_INPUT , ErrorCodes.INVALID_INPUT));
	
		String bodyOfResponse = e.getMessage();
        return new ResponseEntity(bodyOfResponse, h, s);
	
	}*/
	
	 @ExceptionHandler(MethodArgumentNotValidException.class)
	 public ResponseEntity<String> invalidInput(MethodArgumentNotValidException ex) {
	       return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error");
	 }
	
	/*
	 * Exception Handler for Hibernate Related Exception
	 */
	
	@ExceptionHandler(HibernateException.class)
	public ResponseEntity<ErrorResponseDTO> hibernateHandleException(HibernateException e){
		
		log.error("Exception :: HibernateException :: "+e.getMessage());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponseDTO(ErrorMessages.HIBERNATE_EXCEPTION , ErrorCodes.INTERNAL_SERVER_ERROR));
	}
	
	
	/*
	 * Exception Handler for AlreadyFriendsException
	 */
	
	@ExceptionHandler(DuplicateFriendsException.class)
	public ResponseEntity<ErrorResponseDTO> alreadyFriendsException(DuplicateFriendsException e){
		
		log.error("Exception :: AlreadyFriendsException :: "+e.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponseDTO(e.getMessage(), e.getErrorCode()));
	}

	/*
	 * Exception Handler for InvalidEmailException
	 * All input email related validation exception will be handled by this handler
	 */
	
	@ExceptionHandler(InvalidEmailException.class)
	public ResponseEntity<ErrorResponseDTO> invalidEmailException(InvalidEmailException e){
		
		log.error("Exception :: InvalidEmailException :: "+e.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponseDTO(e.getMessage(), e.getErrorCode()));
	}
	
	
	
	/*
	 * Exception Handler for InvalidEmailException
	 * Error related to Input email that doesn't exist in the system 
	 */
	
	@ExceptionHandler(InvalidUserException.class)
	public ResponseEntity<ErrorResponseDTO> invalidUserHandler(InvalidUserException e){
		
		log.error("Exception :: InvalidUserException :: "+e.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponseDTO(e.getMessage(), e.getErrorCode()));
	}
	
	
	/*
	 * Exception handler for InvalidEmailListException 
	 */
	
	@ExceptionHandler(InvalidEmailListException.class)	
	public ResponseEntity<ErrorResponseDTO> invalidEmailListHandler(InvalidUserException e){
		
		log.error("Exception :: InvalidEmailListException :: "+e.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponseDTO(e.getMessage(), e.getErrorCode()));
	}
	
	
	/*
	 * Exception handler for AlreadySubscribedException exception
	 */
	
	@ExceptionHandler(DuplicateSubscribedException.class)
	public ResponseEntity<ErrorResponseDTO> alreadySubscribedHandler(DuplicateSubscribedException e){
		
		log.error("Exception :: AlreadySubscribedException :: "+e.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponseDTO(e.getMessage(), e.getErrorCode()));
	}
	
	
	/*
	 * Exception handler for AlreadySubscribedException exception
	 */
	
	@ExceptionHandler(DuplicateBlockException.class)
	public ResponseEntity<ErrorResponseDTO> alreadyBlockedExceptionHandler(DuplicateSubscribedException e){
		
		log.error("Exception :: AlreadySubscribedException :: "+e.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponseDTO(e.getMessage(), e.getErrorCode()));
		
	}
	
	
	/*
	 * Exception handler for UserBlcokedExeption exception
	 */
	
	@ExceptionHandler(UpdatesBlockedExeption.class)
	public ResponseEntity<ErrorResponseDTO> userBlockedExceptionHandler(UpdatesBlockedExeption e){
		
		log.error("Exception :: AlreadySubscribedException :: "+e.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponseDTO(e.getMessage(), e.getErrorCode()));
		
	}
	
	
	/*
	 * Exception handler for generic exception
	 */
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponseDTO> genericExceptionHandler(Exception e){
		
		log.error("Exception :: Exception :: "+e.getMessage());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponseDTO(ErrorMessages.INTERNAL_SERVER_ERROR, ErrorCodes.INTERNAL_SERVER_ERROR));
	
	}
}