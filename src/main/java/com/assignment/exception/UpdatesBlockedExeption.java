package com.assignment.exception;

import com.assignment.constants.ErrorCodes;
import com.assignment.constants.ErrorMessages;

/*
 *  Exception thrown when attempt to create friends between blocked user
 */

public class UpdatesBlockedExeption extends BaseException {
	
	private static final long serialVersionUID = 1874689585785213423L;

	public  UpdatesBlockedExeption(String message) {
		
		super(message);
	}
	
	public  UpdatesBlockedExeption(String requestor, String target) {
		
		super(requestor+ ErrorMessages.USER_BLOCKED+" "+target+". "+ErrorMessages.NEW_FRIENDS_ERROR);
	}
	
	@Override
	public String getErrorCode() {
		
		return ErrorCodes.USER_BLOCKED;
	
	}

}
