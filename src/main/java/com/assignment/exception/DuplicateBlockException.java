package com.assignment.exception;

import com.assignment.constants.ErrorCodes;

public class DuplicateBlockException extends BaseException {

	
	private static final long serialVersionUID = -5751487624178031107L;

	public DuplicateBlockException(String message){
		super(message);
	}
	
	@Override
	public String getErrorCode() {
		return ErrorCodes.ALREADY_BLOCKED_ERROR;
	}

}
