package com.assignment.exception;

import com.assignment.constants.ErrorCodes;
import com.assignment.constants.ErrorMessages;

public class InvalidEmailListException extends BaseException{

	private static final long serialVersionUID = 2185193476822488934L;

	public InvalidEmailListException(String message){
		super(ErrorMessages.INVALID_EMAIL_LIST+" "+message);
	}
	
	@Override
	public String getErrorCode(){
		return ErrorCodes.INVALID_EMAIL_INPUT;
	}
}
