package com.assignment.exception;

import com.assignment.constants.ErrorCodes;

public class InvalidEmailException extends BaseException {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidEmailException(String message){
		super(message);
	}
	
	@Override
	public String getErrorCode(){
		return ErrorCodes.INVALID_EMAIL_INPUT;
	}
}
