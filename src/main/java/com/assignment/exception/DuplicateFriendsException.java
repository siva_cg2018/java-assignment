package com.assignment.exception;

import com.assignment.constants.ErrorCodes;

public class DuplicateFriendsException extends BaseException {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DuplicateFriendsException(String message){
		super(message);
	}
	
	@Override
	public String getErrorCode(){
		return ErrorCodes.ALREADY_FRIENDS_ERROR;
	}
	
}
