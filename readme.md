# FRIENDS MANAGAMENT - ASSIGNMENT

[![N|Solid](HTTPs://cldup.com/dTxpPi9lDf.thumb.png)](HTTPs://nodesource.com/products/nsolid)

[![Build Status](HTTPs://travis-ci.org/joemccann/dillinger.svg?branch=master)](HTTPs://travis-ci.org/joemccann/dillinger)

Friends management is a simple application to manage friends connnection between users, and also subscribe and block updates from user  

## Technology used

### Spring 
1. Spring provides a well designed MVC framework, to build web application quite eaisly
2. Easy dependency injection management using Autowried annotation

### Hibernate
1. Hibernate is a Java based Object Relation Mapping(ORM) to handle database related action 

### MySQL Database
1. MySQL is a lightweight RDBMS which is compatible with hibernate


## Installation

1. Download the source code from the repository
2. In eclipse import as a Maven Project
3. Install all the dependencies, by running as Maven Build
4. Start the server to access the APIs


## REST API Endpoints explanation


Base URL = HTTP://localhost:<port_number>/RESTAPI/<path>

1. Create friends connection between 2 user
    
    -Path : /new_friends
    -Method : POST
    -Headers : Content-type : Application/JSON
    
    -Input : 
    
    ``` 
    
    {
        "friends" : [
            "john@example.com",
            "andy@example.com"
        ]
    }
    
    ```
    
    -Expected Output
    
    ```
     
     {
         "success" : true
     }
    
    ```
    
    - Error code and messages in case of user related issues
    
    ```
    
    Invalid email provided
    40001 - Invalid email id format or invalid number of email id provided
    HTTP Status - Bad Request
    
    2 users are already friend
    40011 - Already friends
    HTTP Status - Bad Request
    
    ```

2. Retrieve friend list for a user email
    
    -Path : /friends_list
    -Method : POST
    -Headers : Content-type : Application/JSON
    
    -Input : 
    
    ``` 
    
    {
        "email" : "john@example.com"
    }
    
    ```
    
    -Expected Output
    
    ```
     
     {   
     	"success": true,   
     	"friends" :[
     					'john@example.com'     
     				],   
     	"count" : 1    
     } 
    
    ```
    
    - Error code and messages in case of user related issues
    
    
    ```
    
    Invalid email provided
    40001 - Invalid email id format | Email id missing or null
    HTTP Status - Bad Request
    
    ```
 
3. Retrieve the common friends list between two email addresses
    
    -Path : /get_common_friends
    -Method : POST
    -Headers : Content-type : Application/JSON
    
    -Input : 
    

    ``` 
    
    {
        "friends" : [
            "john@example.com",
            "andy@example.com"
        ]
    }
    
    
    ```
    
    -Expected Output
    
    
    ```
    
     {   
     	"success": true,   
     	"friends" :[
     					'common@example.com'     
     				],   
     	"count" : 1    
     } 
    
    ```
    
    - Error code and messages in case of user related issues
    
    
    ```
    
    Invalid email provided
    40001 - Invalid email id format | Email id missing or null
    HTTP Status - Bad Request
    
    ```
 
 
 4. Subscribe to updates from an email address
    
    -Path : /subscribe_updates
    -Method : POST
    -Headers : Content-type : Application/JSON
    
    -Input : 
    
    ``` 
    
    {   
    	"requestor": "lisa@example.com",   
    	"target": "john@example.com" 
    } 
    
    ```
    
    -Expected Output
    
    ```
    
     {   
     	"success": true   
     } 
    
    ```
    
    - Error code and messages in case of user related issues
    
    
    ```
    
    Invalid email provided
    40001 - Invalid email id format | Email id missing or null
    HTTP Status - Bad Request
    
    Already Subscribed
    40012 - Requestor has already subscribed for updates from the target
    HTTP Status  - Bad Request
    
    ```
 

5. Retrieve all email addresses that can receive updates from an email address
    
    -Path : /block_updates
    -Method : POST
    -Headers : Content-type : Application/JSON
    
    -Input : 
    
    
    ``` 
    
    {   
    	"requestor": "andy@example.com",   
    	"target": "john@example.com" 
    } 
    
    ```
    
    -Expected Output
    
    ```
     
     {   
     	"success": true   
     } 
    
    ```
    
    - Error code and messages in case of user related issues
    
    
    ```
    
    Invalid email provided
    40001 - Invalid email id format | Email id missing or null
    HTTP Status - Bad Request
    
    Already Subscribed
    40012 - Requestor has already blocked the target
    HTTP Status  - Bad Request
    
    
    ```
    
 6. Block updates from an email address
    
    -Path : /send_updates
    -Method : POST
    -Headers : Content-type : Application/JSON
    
    -Input : 
    
    
    ``` 
    
    {   
    	"sender": "john@example.com",   
    	"text": "Hello sir kate@example.com" 
    } 
    
    ```
    
    -Expected Output
    
    ```
     
     {   
     	"success": true,
     	"recipients": 
     		[ 
     				"lisa@example.com",       
     				"kate@example.com"     
     		]    
     } 
    
    ```
    
    - Error code and messages in case of user related issues
    
    
    ```
    
    Invalid email provided
    40001 - Invalid email id format | Email id missing or null | Both email cannot be same
    HTTP Status - Bad Request
    
    ```
    
    
 ## Generic Error Messages
 
 	Error response format
 	
 	```
 	
 	{
 		"success" : false,
 		 "message" : ERROR MESSAGE,
 		 "error_code" : ERROR_CODE
 	}
 	
 
 	Invalid URL Request 
	404 - Requested resource doesn't exist
	HTTP Status - Not Found

	Invalid request body provided
	401 - Invalid request body
	HTTP Status - Bad Request
 
 	```
 	
 	
 	
## Database ER diagram
![DB ER DIAGM](https://bitbucket.org/siva_cg2018/java-assignment/raw/a8703021d94ec2ae3a6bdb7dc157bac49af83b33/others/FriendsManagement-ER.png)